<?php
Class Covert{
    /**
     * WGS84与百度坐标系 (BD-09) 的转换
     */
    function wgs84tobd09($lng, $lat)
    {
        $cj2 = $this->wgs84togcj02($lng, $lat);
        echo json_encode($this->gcj02tobd09($cj2[0], $cj2[1]));die;
        return $this->gcj02tobd09($cj2[0], $cj2[1]);
    }
    /**
     * 火星坐标系 (GCJ-02) 与百度坐标系 (BD-09) 的转换
     * 即谷歌、高德 转 百度
     */
    function gcj02tobd09($lng, $lat)
    {
        $x_PI = 3.14159265358979324 * 3000.0 / 180.0;
        $lat = +$lat;
        $lng = +$lng;
        $z = sqrt($lng * $lng + $lat * $lat) + 0.00002 * sin($lat * $x_PI);
        $theta = atan2($lat, $lng) + 0.000003 * cos($lng * $x_PI);
        $bd_lng = $z * cos($theta) + 0.0065;
        $bd_lat = $z * sin($theta) + 0.006;
        return [$bd_lng, $bd_lat];
    }
    /**
     * WGS84转GCj02
     */
    function wgs84togcj02($lng, $lat)
    {
        $ee = 0.00669342162296594323;
        $a = 6378245.0;
        $lat = +$lat;
        $lng = +$lng;
        if ($this->out_of_china($lng, $lat)) {
            return [$lng, $lat];
        } else {
            $dlat = $this->transformlat($lng - 105.0, $lat - 35.0);
            $dlng = $this->transformlng($lng - 105.0, $lat - 35.0);
            $radlat = $lat / 180.0 * M_PI;
            $magic = sin($radlat);
            $magic = 1 - $ee * $magic * $magic;
            $sqrtmagic = sqrt($magic);
            $dlat = ($dlat * 180.0) / (($a * (1 - $ee)) / ($magic * $sqrtmagic) * M_PI);
            $dlng = ($dlng * 180.0) / ($a / $sqrtmagic * cos($radlat) * M_PI);
            $mglat = $lat + $dlat;
            $mglng = $lng + $dlng;
            return [$mglng, $mglat];
        }
    }
    function transformlat($lng, $lat)
    {
        $lat = +$lat;
        $lng = +$lng;
        $ret = -100.0 + 2.0 * $lng + 3.0 * $lat + 0.2 * $lat * $lat + 0.1 * $lng * $lat + 0.2 * sqrt(abs($lng));
        $ret += (20.0 * sin(6.0 * $lng * M_PI) + 20.0 * sin(2.0 * $lng * M_PI)) * 2.0 / 3.0;
        $ret += (20.0 * sin($lat * M_PI) + 40.0 * sin($lat / 3.0 * M_PI)) * 2.0 / 3.0;
        $ret += (160.0 * sin($lat / 12.0 * M_PI) + 320 * sin($lat * M_PI / 30.0)) * 2.0 / 3.0;
        return $ret;
    }

    function transformlng($lng, $lat)
    {
        $lat = +$lat;
        $lng = +$lng;
        $ret = 300.0 + $lng + 2.0 * $lat + 0.1 * $lng * $lng + 0.1 * $lng * $lat + 0.1 * sqrt(abs($lng));
        $ret += (20.0 * sin(6.0 * $lng * M_PI) + 20.0 * sin(2.0 * $lng * M_PI)) * 2.0 / 3.0;
        $ret += (20.0 * sin($lng * M_PI) + 40.0 * sin($lng / 3.0 * M_PI)) * 2.0 / 3.0;
        $ret += (150.0 * sin($lng / 12.0 * M_PI) + 300.0 * sin($lng / 30.0 * M_PI)) * 2.0 / 3.0;
        return $ret;
    }
    /**
     * 判断是否在国内，不在国内则不做偏移
     */
    function out_of_china($lng, $lat)
    {
        $lat = +$lat;
        $lng = +$lng;
        // 纬度3.86~53.55,经度73.66~135.05
        return !($lng > 73.66 && $lng < 135.05 && $lat > 3.86 && $lat < 53.55);
    }
}